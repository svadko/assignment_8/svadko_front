/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["localhost", "46.101.124.56"],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**",
      },
    ],
  },
};

module.exports = nextConfig;
