import {all} from 'redux-saga/effects';
import {booksSagas} from './booksSagas';

export default function* rootSaga(){
    yield all([
        booksSagas()
    ])
}