import Banner from "./pageModules/Parts/Banner";
import Header from "./pageModules/Parts/Header";
import PageTemplate from "./pageModules/PageTemplate";


export default function Home() {
  return (
      <div>
        <Header></Header>
        {/* <Banner></Banner> */}
        <PageTemplate />
      </div>
  );
}
