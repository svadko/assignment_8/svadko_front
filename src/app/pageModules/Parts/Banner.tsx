import styles from "../pageThemes/main.module.css";

export default function Banner() {
  return (
    <div className={styles.Banner}>
      <div className={styles.InnerBanner}></div>
    </div>
  );
}
