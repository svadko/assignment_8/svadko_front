import { title } from "process";
import styles from "../pageThemes/main.module.css";
import Icon_1 from "../Icons/Icon_1";
import Icon_2 from "../Icons/Icon_2";
import Link from "next/link";
import Left from "./Left";
import axios from "axios";
import { useEffect } from "react";


const pages = [
  { title: "Детективы", path: "/page/det/1", select: "/det" },
  { title: "Романы", path: "/page/rom/1", select: "/rom" },
  { title: "Триллеры", path: "/page/tr/1", select: "/tr" },
  { title: "Приключения", path: "/page/ad/1", select: "/ad" },
  { title: "Комедии", path: "/page/cm/1", select: "/cm" },
  { title: "Истории", path: "/page/hi/1", select: "/hi" },
  { title: "Обучение", path: "/page/ed/1", select: "/ed" },
  { title: "Журналы", path: "/page/jr/1", select: "/jr" },
  { title: "Манга", path: "/page/mg/1", select: "/mg" },
  { title: "Иностранное", path: "/page/en/1", select: "/en" },
];

export default function Header() {  
  return (
    <div>
      <div className={styles.InnerHeader}>
        <Link href={""}>
          <p>Казахстан</p>
        </Link>
        <Link href={""}>
          <p>Где мой заказ?</p>
        </Link>{" "}
        <Link href={""}>
          <p>О нас</p>
        </Link>
      </div>
      <div className={styles.InnerHeader}>
        <Icon_1></Icon_1>
        {/* <h1 className={styles.sva}>SVA:DKO</h1> */}
        <div className={styles.logo_name}>
          <Icon_2></Icon_2>
        </div>
        <h1>КАТАЛОГ</h1>
        <h1>ОБМЕН</h1>
        <h1>ГОЛОСОВАНИЕ</h1>
      </div>
      <div className={styles.InnerHeader}>
        {pages.map(({ title, path, select }, index) => (
          <Link key={index} href={path}>
            <h4>{title}</h4>
          </Link>
        ))}
      </div>
    </div>
  );
}
