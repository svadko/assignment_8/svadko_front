export default function Icon_1() {
  return (
    <svg
      width="32"
      height="49"
      viewBox="0 0 32 49"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_1_33)">
        <path
          d="M10.8572 0.160735C11.0702 -0.0535783 11.4155 -0.0535783 11.6285 0.160735L27.8277 16.4588C28.0407 16.6731 28.0407 17.0205 27.8277 17.2349L27.0563 18.0109C26.8433 18.2253 26.4979 18.2253 26.2849 18.0109L10.0858 1.71293C9.87275 1.49861 9.87275 1.15114 10.0858 0.936831L10.8572 0.160735Z"
          fill="#E629AB"
        />
        <path
          d="M4 18.5685C4 17.6214 4.76315 16.8536 5.70455 16.8536H28V39.2851C28 40.2322 27.2368 41 26.2955 41H5.70455C4.76315 41 4 40.2322 4 39.2851V18.5685Z"
          fill="#E629AB"
        />
      </g>
      <defs>
        <filter
          id="filter0_d_1_33"
          x="0"
          y="0"
          width="32"
          height="49"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_1_33"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_1_33"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
}
